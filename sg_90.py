import RPi.GPIO as GPIO
import time

SERVO = 32

GPIO.setmode(GPIO.BOARD)
GPIO.setup(SERVO, GPIO.OUT)

def set_angle(angle):

    duty = angle / 18 + 2
    GPIO.output(SERVO, True)
    PWM.ChangeDutyCycle(duty)
    time.sleep(1)
    GPIO.output(SERVO, False)
    PWM.ChangeDutyCycle(0)


def up():
    set_angle(0)


def down():
    set_angle(90)


def init():
    global PWM
    PWM = GPIO.PWM(SERVO, 50)
    PWM.start(0)
    set_angle(90)
