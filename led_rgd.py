import RPi.GPIO as GPIO
import time


GREEN = 37
YELLOW = 38
RED = 40

ON = 1
OFF = 0


def turnOnGreen():
    print('> Ligando led verde.')
    GPIO.output(GREEN, ON)

def turnOnYellow():
    print('> Ligando led amarelo.')
    GPIO.output(YELLOW, ON)

def turnOnRed():
    print('> Ligando led vermelho.')
    GPIO.output(RED, ON)

def turnOffAll():
    print('> Desligando todos os leds.')
    GPIO.output(GREEN, OFF)
    GPIO.output(YELLOW, OFF)
    GPIO.output(RED, OFF)

def cleanSetup():
    print('> Removendo configuracoes das pinagens.')
    GPIO.cleanup(GREEN)
    GPIO.cleanup(YELLOW)
    GPIO.cleanup(RED)

