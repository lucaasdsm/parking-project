import json
import requests
import datetime


URL_SLACK = 'https://hooks.slack.com/services/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'
PAYLOAD = ''

def create_message(message):

    global PAYLOAD

    PAYLOAD = json.dumps({  
        'text':  '```'+ message + '```'
    })

def check_status(r):

    if r.status_code == 200:
        print('>    Funcionou! / STATUS: ' + str(r.status_code))
    else:
        print('>    Deu ruim... / STATUS: ' + str(r.status_code))


def send_message(message):

    try:
      create_message(message)
      #print(PAYLOAD)
      print('> Inicio da conexao: ' + str(datetime.datetime.now()) + '\n')
      r = requests.post(URL_SLACK, data=PAYLOAD)
      check_status(r)
      return r
    except requests.exceptions.RequestException as e:
      print('> Conexao recusada. \n')
