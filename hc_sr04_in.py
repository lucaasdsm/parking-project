import RPi.GPIO as GPIO
import time


TRIGGER = 18
ECHO = 16

def check():
    GPIO.output(TRIGGER, True)
    time.sleep(0.00001)
    GPIO.output(TRIGGER, False)

    startTime = time.time()
    stopTime = time.time()

    while 0 == GPIO.input(ECHO):
        startTime = time.time()

    while 1 == GPIO.input(ECHO):
        stopTime = time.time()

    timeElapsed = stopTime - startTime
    distance = (timeElapsed * 34300) / 2

    return distance