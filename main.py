import RPi.GPIO as GPIO
import sg_90
import hc_sr04_in
import hc_sr04_out
import led_rgd
import lcddriver
import time
import mongodb
from datetime import datetime
from slack import send_message


MIN_DISTANCE = 20
VACANCIES = 30
MESSAGE = ''
SEMAPHORE = led_rgd
ULTRASONIC_IN = hc_sr04_in
ULTRASONIC_OUT = hc_sr04_out
SERVO_MOTOR = sg_90
LCD = lcddriver.lcd()
now = datetime.now()
CURRENT_TIME = now.strftime('%d/%m/%y %H:%M')


LAST_TIME = now.strftime('%d/%m/%y %H:%M')


def init():

    GPIO.setmode(GPIO.BOARD)

    ECHO_IN = 16
    TRIGGER_IN = 18
    ECHO_OUT = 11
    TRIGGER_OUT = 13

    SERVO = 32

    GREEN = 37
    YELLOW = 38
    RED = 40

    ON = 1
    OFF = 0

    GPIO.setup(TRIGGER_IN, GPIO.OUT)
    GPIO.setup(ECHO_IN, GPIO.IN)
    GPIO.setup(TRIGGER_OUT, GPIO.OUT)
    GPIO.setup(ECHO_OUT, GPIO.IN)

    GPIO.setup(SERVO, GPIO.OUT)

    GPIO.setup(RED, GPIO.OUT)
    GPIO.setup(GREEN, GPIO.OUT)
    GPIO.setup(YELLOW, GPIO.OUT)

    write_lcd(' ')

    SERVO_MOTOR.init()


def loop():

    global MIN_DISTANCE
    global VACANCIES
    global SEMAPHORE
    global ULTRASONIC_IN
    global ULTRASONIC_OUT
    global SERVO_MOTOR
    global LCD
    global CURRENT_TIME

    while True:
        update_lcd()
        SEMAPHORE.turnOnRed()

        distance_in = ULTRASONIC_IN.check()
        distance_out = ULTRASONIC_OUT.check()

        if distance_in < MIN_DISTANCE:

            SEMAPHORE.turnOffAll()
            SEMAPHORE.turnOnYellow()
            time.sleep(1)

            SEMAPHORE.turnOffAll()
            SEMAPHORE.turnOnGreen()

            SERVO_MOTOR.up()

            while (distance_in < MIN_DISTANCE):
                distance_in = ULTRASONIC_IN.check()
                print('>    distance_in : ' + str(distance_in))
                time.sleep(0.5)

            VACANCIES = VACANCIES - 1
            print('>    Vagas: ' + str(VACANCIES))

            mongodb.entry(VACANCIES)

            write_lcd('Entrando veiculo...')
            create_message('Entrada')

            SEMAPHORE.turnOffAll()
            time.sleep(1)

            write_lcd(' ')
            SERVO_MOTOR.down()

        if distance_out < MIN_DISTANCE:

            SEMAPHORE.turnOffAll()
            SEMAPHORE.turnOnYellow()
            time.sleep(1)

            SEMAPHORE.turnOffAll()
            SEMAPHORE.turnOnGreen()

            SERVO_MOTOR.up()

            while (distance_out < MIN_DISTANCE):
                distance_out = ULTRASONIC_OUT.check()
                print('>    distance_out : ' + str(distance_out))
                time.sleep(0.5)

            VACANCIES = VACANCIES + 1
            print('>    Vagas: ' + str(VACANCIES))

            mongodb.exit(VACANCIES)

            write_lcd('Saindo veiculo...')
            create_message('Saída')

            SEMAPHORE.turnOffAll()
            time.sleep(1)

            write_lcd(' ')
            SERVO_MOTOR.down()

        time.sleep(1)


def create_message(type):

    global MESSAGE
    now = datetime.now()

    MESSAGE  = 'TIPO   : ' + type + '\n'
    MESSAGE += 'DATA   : ' + now.strftime('%d/%m/%Y %H:%M:%S') + '\n'
    MESSAGE += 'VAGAS  : ' + str(VACANCIES)

    print(MESSAGE)
    send_message(MESSAGE)


def update_lcd():
    write_lcd('')


def write_lcd(text):

    global LCD
    global CURRENT_TIME
    global LAST_TIME

    now = datetime.now()
    CURRENT_TIME = now.strftime('%d/%m/%y %H:%M')

    if (CURRENT_TIME != LAST_TIME or text != ''):
        LCD.lcd_clear()
        LCD.lcd_display_string('     BEM-VINDO     ', 1)
        LCD.lcd_display_string('VAGAS: ' +str(VACANCIES), 2)
        LCD.lcd_display_string('DATA: ' + now.strftime('%d/%m/%y %H:%M'), 3)
        LCD.lcd_display_string(text, 4)
        LAST_TIME = CURRENT_TIME


def cleanup():

    global LCD

    LCD.lcd_clear()
    print('> Removendo as configuracoes das pinagens.')
    GPIO.cleanup(11)
    GPIO.cleanup(13)
    GPIO.cleanup(16)
    GPIO.cleanup(18)
    GPIO.cleanup(32)
    GPIO.cleanup(37)
    GPIO.cleanup(38)
    GPIO.cleanup(40)


def main():

    init()
    try:
        loop()
    except KeyboardInterrupt:
        print('>>>>>>>>> Interrompendo execucao!')
    finally:
        cleanup()


if __name__ == '__main__':
    main()

