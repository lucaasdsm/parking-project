from pymongo import MongoClient
import urllib 
import datetime

CLUSTER_ADDRESS = '@cluster0-mylfu.mongodb.net/test?retryWrites=true&w=majority'
USER = 'omnistack'
PASSWORD = urllib.parse.quote_plus('abc@123')

def entry(vacancies):
    client = MongoClient('mongodb+srv://' + USER +':' + PASSWORD + CLUSTER_ADDRESS)
    db = client.park
    collection = db.logs
    post = {'tipo': 'Entrada',
        'vagas': vacancies,
        'data': datetime.datetime.utcnow()}
    post_id = collection.insert_one(post).inserted_id


def exit(vacancies):
    client = MongoClient('mongodb+srv://omnistack:' + urllib.parse.quote_plus('abc@123') + CLUSTER_ADDRESS)
    db = client.park
    collection = db.logs
    post = {'tipo': 'Saída',
        'vagas': vacancies,
        'data': datetime.datetime.utcnow()}
    post_id = collection.insert_one(post).inserted_id


def init():
    client = MongoClient('mongodb+srv://omnistack:' + urllib.parse.quote_plus('abc@123') + CLUSTER_ADDRESS)
    db = client.park
    collection = db.logs
